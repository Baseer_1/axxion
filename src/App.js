import 'bootstrap/dist/css/bootstrap.min.css';

import "./App.css";

import MainLayout from "./screens/Layout/layout";
function App() {
  return (
    <>
      <MainLayout />
    </>
  );
}

export default App;
