import React from "react";
import { Dropdown, DropdownButton } from "react-bootstrap";
import "./style.scss";

export default function Header() {
  return (
    <div className="header_section">
      <p> AXXION</p>
      <div className="user">
        <Dropdown className="header_dropdown">
          <Dropdown.Toggle id="dropdown-basic">Supervisor</Dropdown.Toggle>

          <Dropdown.Menu>
            <Dropdown.Item href="#/action-1">Profile</Dropdown.Item>
            <Dropdown.Item href="#/action-2">Settings</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </div>
    </div>
  );
}
