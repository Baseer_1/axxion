import React from "react";
import { Form } from "react-bootstrap";
import "./style.scss";

export default function Inputfield({
  width,
  borderColor,
  type,
  label,
  placeholder,
  icon,
  iconName
}) {
  return (
    <Form.Group className="form_input_group" controlId="formBasicEmail">
      {label && <Form.Label className="lbl">{label}</Form.Label>}
      <div style={{ borderColor, width }} className="input_div">
       {icon && <i  className={iconName}></i>}
        <Form.Control
          className="form_cntrl"
          type={type}
          placeholder={placeholder}
        />
      </div>
    </Form.Group>
  );
}
