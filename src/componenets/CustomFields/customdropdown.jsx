import React from "react";
import { Form } from "react-bootstrap";
import "./style.scss";

export default function CustomDropDown({
  defaultValue,
  label,
  placholder,
  data,
  borderColor,
  width
}) {
  return (
    <Form.Group controlId="formGroupEmail">
      {label && <Form.Label className="cstm_lable">{label}</Form.Label>}
      <Form.Select
      style={{borderColor,width}}
        className="custom_select"
        aria-label="Default select example"
      >
        <option className="slct_options">{placholder}</option>
        {Object.keys(data).map(function (key) {
          return (
            <option className="slct_options" defaultValue={defaultValue} value={key}>
              {data[key]}
            </option>
          );
        })}
      </Form.Select>
    </Form.Group>
  );
}
