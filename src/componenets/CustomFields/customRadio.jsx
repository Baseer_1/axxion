import React from "react";
import { Form } from "react-bootstrap";
import "./style.scss";

export default function CustomRadio({ title, img,  }) {
  return (
    <div className="radio_div">
      <div className="radio_img_div">
        <img src={img} className="radio_img" />
      </div>
      <p className="radio_title">{title}</p>
      <Form.Check className="radio-icon" name="floorLevel" type="radio" aria-label="radio 1" />
    </div>
  );
}
