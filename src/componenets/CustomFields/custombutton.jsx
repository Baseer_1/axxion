import React from "react";
import { Button } from "react-bootstrap";
import "./style.scss";

export default function CustomButton({width,height, text, icon, img, iconName }) {
  return (
    <Button style={{width, height}} className="custom_button" size="lg">
      {img && <img className="bttn_img" src={img} alt="icon" />}
      {icon && <i className={iconName}></i>}
      {text}
    </Button>
  );
}
