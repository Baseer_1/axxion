import React from "react";
import { ListGroup } from "react-bootstrap";
import "./style.scss";

import dashboard from "../../Assets/images/dashboard.svg";
import calendar from "../../Assets/images/calendar.svg";
import timelapse from "../../Assets/images/time-lapse.svg";
import company from "../../Assets/images/company.svg";

export default function Sidebar() {
  return (
    <div className="sidebar_section">
      <ListGroup>
      <ListGroup.Item action href="#link1">
          <div className="sidebar_item">
            <img src={dashboard} width="20px" />
            {/* <i class="fas fa-th-large"></i> */}
            <p>Dashboard</p>
          </div>
        </ListGroup.Item>
        <ListGroup.Item action href="#link2">
          <div className="sidebar_item">
            <img src={calendar} width="20px" />
            {/* <i class="far fa-calendar-alt"></i> */}
            <p>Date Selection</p>
          </div>
        </ListGroup.Item>
        <ListGroup.Item action href="#link3">
          <div className="sidebar_item">
            <img src={timelapse} width="20px" />
            {/* <i class="fa fa-calendar-times-o" aria-hidden="true"></i> */}
            <p>Time Lapse</p>
          </div>
        </ListGroup.Item>
        <ListGroup.Item action href="#link4">
          <div className="sidebar_item">
          {/* <i class="fas fa-building"></i> */}
            <img src={company} width="20px" />
            <p>Company</p>
          </div>
        </ListGroup.Item>
        <ListGroup.Item action href="#link5">
          <div className="sidebar_item">
            <img src={company} width="20px" />
            
            <p>Floor Level</p>
          </div>
        </ListGroup.Item>
      
      </ListGroup>
    </div>
  );
}
