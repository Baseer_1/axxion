import React from "react";
import { Form, Row, Col, Container, Table } from "react-bootstrap";
import CustomButton from "../../componenets/CustomFields/custombutton";
import CustomDropdown from "../../componenets/CustomFields/customdropdown.jsx";
import floorMap from "../../Assets/images/floorMap.png";
import { CircularProgressbar } from "react-circular-progressbar";
import { CircularProgressbarWithChildren } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import star from "../../Assets/images/star.svg";
import pdf from "../../Assets/images/pdf.svg";
import usersgroup from "../../Assets/images/usersgroup.svg";

import "./style.scss";

export default function Dashboard() {
  const percentage = 66;
  return (
    <div className="dashboard_section">
      <div className="SubHeader">
        <div>
          <p className="heading"> Dashboard </p>
        </div>
      </div>

      <Container className="mainContent">
        <Row className="justify-content-center">
          <Col md={6}  >
            <div className="dashboard_box firstBox">
              <div className="searchBox">
                <Form.Group controlId="formBasicEmail">
                  <Form.Control type="text" placeholder="Search user" />
                </Form.Group>
              </div>
              <div className="circle_div">
                <div className="circles">
                  <CircularProgressbarWithChildren
                    value={66}
                    styles={{ path: { stroke: "#FCAD3A" } }}
                  >
                    <div className="circleText">
                      <p>1300</p>
                      <p>Workers on this Site</p>
                      <p>87%</p>
                    </div>
                  </CircularProgressbarWithChildren>
                </div>
                <div className="circles">
                  <CircularProgressbarWithChildren
                    value={66}
                    styles={{ path: { stroke: "#62D39D" } }}
                  >
                    <div className="circleText">
                      <p>1300</p>
                      <p>Workers on this Site</p>
                      <p>87%</p>
                    </div>
                  </CircularProgressbarWithChildren>
                </div>
                <div className="circles">
                  <CircularProgressbarWithChildren
                    value={66}
                    styles={{ path: { stroke: "#D43464" } }}
                  >
                    <div className="circleText">
                      <p>1300</p>
                      <p>Workers on this Site</p>
                      <p>87%</p>
                    </div>
                  </CircularProgressbarWithChildren>
                </div>
              </div>
            </div>
          </Col>
          <Col md={6} className="" >
            <div className=" dashboard_box secondBox">
              <p className="heading">Top Labour Hours</p>
              <div className="trackBox">
                <div className="track">
                  <div className="star" style={{ backgroundColor: "#C7C5C4" }}>
                    <img src={star} alt="" srcset="" />
                  </div>
                  <div className="data">
                    <div className="text">Day (Insert Date)</div>
                    <div className="text">|</div>
                    <div className="text">Hours: 400</div>
                  </div>
                </div>
                <div className="track">
                  <div className="star" style={{ backgroundColor: "#964B00" }}>
                    <img src={star} alt="" srcset="" />
                  </div>
                  <div className="data">
                    <div className="text">Week</div>
                    <div className="text">|</div>
                    <div className="text">Hours: 400</div>
                  </div>
                </div>
                <div className="track">
                  <div className="star" style={{ backgroundColor: "#D9C35E" }}>
                    <img src={star} alt="" srcset="" />
                  </div>
                  <div className="data">
                    <div className="text">Month (Insert Month)</div>
                    <div className="text">|</div>
                    <div className="text">Hours: 400</div>
                  </div>
                </div>
              </div>
            </div>
          </Col>
          <Col md={6} className="mt-4" >
            <div className="dashboard_box thirdBox ">
              <div className="heading_btn">
                <p className="heading">Day Register</p>
                <CustomButton text="DOWNLOAD .PDF" icon="true" iconPath={pdf} />
              </div>

              <div className="day_table">
              <Table >
                <thead>
                  <tr>
                    
                    <th>CATEGORY</th>
                    <th>NO OF EMP.</th>
                    <th>% OF EMPLOYESS</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>ABC Platters</td>
                    <td>235</td>
                    <td>
                        <div className="progressBox">
                            <div className="current" style={{width:'35%', backgroundColor:'#62D39D'}}>
                                35%
                            </div>
                        </div>
                        
                    </td>
                    </tr>
                    <tr>
                    <td>XYZ Electrical</td>
                    <td>235</td>
                    <td>
                        <div className="progressBox">
                            <div className="current" style={{width:'24%', backgroundColor:'#D24E56'}}>
                                24%
                            </div>
                        </div>
                        
                    </td>
                    </tr>
                    <tr>
                        
                    <td>Electronics</td>
                    <td>9</td>
                    <td>
                        <div className="progressBox">
                            <div className="current" style={{width:'57%', backgroundColor:'#25A1D0'}}>
                                57%
                            </div>
                        </div>
                        
                    </td>
                  </tr>
                  <tr>
                    <td>XYZ Electrical</td>
                    <td>235</td>
                    <td>
                        <div className="progressBox">
                            <div className="current" style={{width:'24%', backgroundColor:'#D24E56'}}>
                                24%
                            </div>
                        </div>
                        
                    </td>
                    </tr>
                    <tr>
                        
                    <td>Electronics</td>
                    <td>9</td>
                    <td>
                        <div className="progressBox">
                            <div className="current" style={{width:'57%', backgroundColor:'#25A1D0'}}>
                                57%
                            </div>
                        </div>
                        
                    </td>
                  </tr>
                  <tr>
                    <td>XYZ Electrical</td>
                    <td>235</td>
                    <td>
                        <div className="progressBox">
                            <div className="current" style={{width:'24%', backgroundColor:'#D24E56'}}>
                                24%
                            </div>
                        </div>
                        
                    </td>
                    </tr>
                    <tr>
                        
                    <td>Electronics</td>
                    <td>9</td>
                    <td>
                        <div className="progressBox">
                            <div className="current" style={{width:'57%', backgroundColor:'#25A1D0'}}>
                                57%
                            </div>
                        </div>
                        
                    </td>
                  </tr>
             
                </tbody>
              </Table>
            </div>
            </div>
            
          </Col>
          <Col md={6} className="" >
            <div className=" dashboard_box fourthBox mt-4">
              <p className="heading">Employess on site</p>
              <div className="circle_div">
                <div className="circles">
                  <CircularProgressbarWithChildren value={66}    styles={{ path: { stroke: "#FCAD3A" } }} >
                    <div style={{ fontSize: 18, marginTop: -5 }}>
                      <strong>70</strong> Days
                    </div>
                  </CircularProgressbarWithChildren>
                </div>
                <div className="circles">
                  <CircularProgressbarWithChildren value={66}  styles={{ path: { stroke: "#62D39D" } }}>
                  
                    <div style={{ fontSize: 12, marginTop: -5 }}>
                      <strong>66%</strong> mate
                    </div>
                  </CircularProgressbarWithChildren>
                </div>
                <div className="circles">
                  <CircularProgressbarWithChildren value={66}  styles={{ path: { stroke: "#D43464" } }}>
                    
                    <div style={{ fontSize: 12, marginTop: -5 }}>
                      <strong>66%</strong> mate
                    </div>
                  </CircularProgressbarWithChildren>
                </div>
              </div>
            </div>
            <div className=" dashboard_box fifthBox mt-4">
                 <p className="heading">Individuals currently on site</p>
                  <div className="groups">
                        <p className="user">65 </p>
                        <img src={usersgroup} alt="groups icon"/>
                  </div>  
            </div>
          </Col>
        
        </Row>
      </Container>
    </div>
  );
}
