import React from "react";
import { Container, Row, Col, Table } from "react-bootstrap";
import CustomButton from "../../componenets/CustomFields/custombutton";
import stairs from "../../Assets/images/stairs.svg";
import teamwork from "../../Assets/images/teamwork.svg";
import floorMap from "../../Assets/images/floorMap.png";
import { Form } from "react-bootstrap";
import "./style.scss";
import CustomRadio from "../../componenets/CustomFields/customRadio";
import Inputfield from "../../componenets/CustomFields/inputfield";

export default function FloorLavel() {
  let data = [
    {
      title: "zaid",
      img: stairs,
    },
    {
      title: "zaid",
      img: stairs,
    },
    {
      title: "zaid",
      img: stairs,
    },
    {
      title: "zaid",
      img: stairs,
    },
    {
      title: "zaid",
      img: stairs,
    },
    {
      title: "zaid",
      img: stairs,
    },
    {
      title: "zaid",
      img: stairs,
    },
  ];
  return (
    <div className="floor_section">
      <div className="floor_header">
        <CustomButton text="ADD NEW FLOOR" icon iconName="fas fa-plus" />
      </div>
      <Container className="floor_content">
        <Row>
          <Col xs={4}>
            <div className="floor_side">
              <Form className="floor_side_form">
                <Inputfield
                  icon
                  iconName="fas fa-search"
                  type="text"
                  placeholder="Search floor no. eg. 55"
                />
                {data.map((data) => (
                  <Col className="mt-4">
                    <CustomRadio img={data.img} title={data.title} />
                  </Col>
                ))}
              </Form>
            </div>
          </Col>
          <Col xs={8}>
            <div className="floor_map_sec">
              <div className="map_title_row">
                <div className="map_title_div">
                  <img src={teamwork} className="map_title_icon" />
                  <p className="title">Employees 8</p>
                </div>
              </div>
              <div className="floor_map_img_div">
                <img src={floorMap} className="floor_map" />
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
