import React from "react";
import { Container, Row, Col, Table } from "react-bootstrap";
import CustomButton from "../../componenets/CustomFields/custombutton";
import pdf from "../../Assets/images/pdf.svg";
import CustomDropdown from "../../componenets/CustomFields/customdropdown";
import { Form } from "react-bootstrap";
import "./style.scss";
import Inputfield from "../../componenets/CustomFields/inputfield";

export default function DateSelectionPage() {
  let floorValue = {
    "01": "Floor : Floor 1",
    "02": "Floor : Floor 2",
    "03": "Floor : Floor 3",
  };
  let companyValue = {
    "01": "ABC Plasters",
    "02": "DEF Plasters",
    "03": "GHI Plasters",
  };

  return (
    <div className="date_section">
      <Row className="date_hdr">
        <Col xs={6} className="title">
          <p> Date Selection</p>
        </Col>
        <Col xs={3} className="options">
          <CustomButton text="DOWNLOAD .PDF" icon iconName="far fa-file" />
        </Col>
        <Col xs={3}>
          <CustomDropdown
            borderColor="#d9c35e"
            placholder="Select Floor "
            data={floorValue}
          />
        </Col>
      </Row>
      <Container className="mainContent">
        <Row className="justify-content-center">
          <Col md={4}>
            <div className="inpt_div">
              <CustomDropdown
                placholder="Select"
                label="Company"
                data={companyValue}
              />
            </div>
            <div className="inpt_div">
              <Inputfield type="text" placeholder="Enter Name" label="Name" />
            </div>
            <div className="inpt_div">
              <Inputfield type="date" label="Date" />
            </div>
          </Col>
          <Col md={4}>
            <div className="inpt_div">
              <CustomDropdown
                placholder="Select Job Title"
                label="Job Title"
                data={companyValue}
              />
            </div>

            <div className="inpt_div">
              <CustomDropdown
                placholder="Select Status"
                label="Status"
                data={companyValue}
              />
            </div>
            <div className="inpt_div">
              <Inputfield type="time" label="Time" />
            </div>
          </Col>
        </Row>
        <Row style={{marginTop: 30}} className="justify-content-center">
          <CustomButton width="150px" text="APPLY" />
        </Row>
        <Row className="justify-content-center table_selection">
          <Col md={8}>
            <Table bordered hover>
              <thead>
                <tr>
                  <th>Company</th>
                  <th>Role</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>ABC Plaster</td>
                  <td>Project Manager</td>
                  <td>Maaria</td>
                  <td>Browning</td>
                </tr>
                <tr>
                  <td>ABC Plaster</td>
                  <td>Project Manager</td>
                  <td>Maaria</td>
                  <td>Browning</td>
                </tr>
                <tr>
                  <td>ABC Plaster</td>
                  <td>Project Manager</td>
                  <td>Maaria</td>
                  <td>Browning</td>
                </tr>
              </tbody>
            </Table>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
