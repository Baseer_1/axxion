import React from "react";
import { Row, Col } from "react-bootstrap";
import Header from "../../componenets/Header";
import Sidebar from "../../componenets/Sidebar";
import DateSelection from "../DateSelection";
import Dashboard from "../Dashboard";
import TimeLapse from "../TimeLapse";
import Company from "../Company";
import { Tab } from "react-bootstrap";
import "./layout.scss";
import FloorLavel from "../FloorLavel";

export default function main() {
  return (
    <div>
      <Header> </Header>

      <Row className="m-0 afterHeader"  >
      <Tab.Container id="list-group-tabs-example" defaultActiveKey="#link1">
        <Col md={3} className="p-0">
          <Sidebar />
        </Col>
        <Col md={9} className="p-0">
          <Tab.Content>
          <Tab.Pane eventKey="#link1">
              <Dashboard />
            </Tab.Pane>
            <Tab.Pane eventKey="#link2">
              <DateSelection />
            </Tab.Pane>
            <Tab.Pane eventKey="#link3">
            <TimeLapse/>
            </Tab.Pane>
            <Tab.Pane eventKey="#link4">
            <Company/>
            </Tab.Pane>
            <Tab.Pane eventKey="#link5">
                <FloorLavel />
              </Tab.Pane>
          </Tab.Content>
        </Col>
        </Tab.Container>
      </Row>
    </div>
  );
}
