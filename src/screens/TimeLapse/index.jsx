import React from "react";
import { Form, Row, Col, Container } from "react-bootstrap";
import CustomDropdown from "../../componenets/CustomFields/customdropdown.jsx";
import floorMap from "../../Assets/images/floorMap.png";
import "./style.scss";
import Inputfield from "../../componenets/CustomFields/inputfield.jsx";

export default function TimeLapse() {
  let floorValue = {
    "01": "Floor : Floor 1",
    "02": "Floor : Floor 2",
    "03": "Floor : Floor 3",
  };

  let dateValue = {
    "01": "February, 6 2021",
    "02": "February, 7 2021",
    "03": "February, 8 2021",
  };
  return (
    <div className="time_lp_section">
      <Row className="header_sec m-0">
        <Col xs={4} className="title">
          <p> Time Lapse</p>
        </Col>
        <Col xs={3} className="options">
        <Inputfield
              icon
              iconName="fas fa-search"
              width="206px"
              borderColor="#d9c35e"
              placeholder="Search By Name"
              type="search"
            />
        </Col>
        <Col xs={3}>
          <CustomDropdown
            borderColor="#d9c35e"
            placholder="Select Date"
            data={dateValue}
          />
        </Col>
        <Col xs={2}>
          <CustomDropdown
            borderColor="#d9c35e"
            placholder="Select Floor "
            data={floorValue}
          />
        </Col>
      </Row>
      <Container className="mainContent">
        <Row className="justify-content-center m-0">
          <Col md={12}>
            <div className="range_box">
              <Form.Range />
              <div className="range_lable">
                <p>12:00 AM</p>
                <p>11:59 PM</p>
              </div>
            </div>
            <div className="image_box">
              <img src={floorMap} alt="map-image" />
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
