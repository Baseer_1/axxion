import React from "react";
import { Form, Row, Col, Container, Table } from "react-bootstrap";
import CustomDropdown from "../../componenets/CustomFields/customdropdown.jsx";
import CustomButton from "../../componenets/CustomFields/custombutton";
import floorMap from "../../Assets/images/floorMap.png";
import "./style.scss";
import Inputfield from "../../componenets/CustomFields/inputfield.jsx";

export default function Company() {
  let dateValue = {
    "01": "ABC Plaster",
    "02": "123 Mechanical",
    "03": "321 Electrical",
  };
  return (
    <div className="cmpny_section">
      <Row className="cmpny_header">
        <Col xs={3} className="title">
          <p>Company</p>
        </Col>
        <Col xs={9}>
          <div className="cmpny_hdr_row">
            <CustomDropdown
              width="160px"
              borderColor="#d9c35e"
              placholder="Select Company"
              data={dateValue}
            />
            <Inputfield
              icon
              iconName="fas fa-search"
              width="206px"
              borderColor="#d9c35e"
              placeholder="Search User"
              type="search"
            />
            <CustomButton text="ADD COMPANY" icon iconName="fas fa-plus" />
            <CustomButton text="ADD USER" icon iconName="fas fa-plus"  />
          </div>
        </Col>
      </Row>
      <Container className="mainContent">
        <Row className="justify-content-center table_selection">
          <Col md={12}>
            <Table bordered hover>
              <thead>
                <tr>
                  <th>Company</th>
                  <th>Job Title</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>ABC Plaster</td>
                  <td>Labourer</td>
                  <td>Maaria</td>
                  <td>Brown</td>
                  <td className="s_active">Active</td>
                  <td>
                    <a href="/"> Edit </a>
                  </td>
                </tr>
                <tr>
                  <td>123 Mechanical</td>
                  <td>Traderperson</td>
                  <td>Jayson</td>
                  <td>Brown</td>
                  <td className="s_offsite">Off-Site</td>
                  <td>
                    <a href="/"> Edit </a>
                  </td>
                </tr>
                <tr>
                  <td>321 Electrical</td>
                  <td>Supervisor</td>
                  <td>Emme</td>
                  <td>Brown</td>
                  <td className="s_inactive">Inactive</td>
                  <td>
                    <a href="/"> Edit </a>
                  </td>
                </tr>

                <tr>
                  <td>ABC Plaster</td>
                  <td>Labourer</td>
                  <td>Maaria</td>
                  <td>Brown</td>
                  <td className="s_active">Active</td>
                  <td>
                    <a href="/"> Edit </a>
                  </td>
                </tr>
                <tr>
                  <td>123 Mechanical</td>
                  <td>Traderperson</td>
                  <td>Jayson</td>
                  <td>Brown</td>
                  <td className="s_offsite">Off-Site</td>
                  <td>
                    <a href="/"> Edit </a>
                  </td>
                </tr>
                <tr>
                  <td>321 Electrical</td>
                  <td>Supervisor</td>
                  <td>Emme</td>
                  <td>Brown</td>
                  <td className="s_inactive">Inactive</td>
                  <td>
                    <a href="/"> Edit </a>
                  </td>
                </tr>
              </tbody>
            </Table>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
